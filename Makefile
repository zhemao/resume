all: cv.pdf resume.pdf

%.pdf: %.tex
	pdflatex $<
